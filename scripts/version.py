#!/usr/bin/env python3
import os
import re

import fire

pre_release_placeholder = 'RC'
version_filepath = os.path.join('.', 'VERSION')
version_pattern = re.compile(fr'^\d+.\d+.\d+(-{pre_release_placeholder})?$')


def get(with_pre_release_placeholder: bool = False):
    with open(version_filepath, 'r') as version_file:
        version_lines = version_file.readlines()
        assert len(version_lines) == 1, 'Version file is malformed'
        version = version_lines[0]
        assert version_pattern.match(version), 'Version string is malformed'
        if with_pre_release_placeholder:
            return version
        else:
            return version.replace("-{}".format(pre_release_placeholder), '')


def write_version_file(major: int, minor: int, patch: int, prod=False):
    version = "{}.{}.{}".format(major, minor, patch)
    if prod is False:
        version += "-{}".format(pre_release_placeholder)
    with open(version_filepath, 'w') as version_file:
        version_file.write(version)


def release_patch():
    version = get()
    major, minor, patch = version.split('.')
    write_version_file(major, minor, int(patch) + 1)


def release_minor():
    version = get()
    major, minor, patch = version.split('.')
    write_version_file(major, int(minor) + 1, 0)


def release_major():
    version = get()
    major, minor, patch = version.split('.')
    write_version_file(int(major) + 1, 0, 0)


def release_production():
    version = get()
    major, minor, patch = version.split('.')
    write_version_file(major, minor, patch, prod=True)


if __name__ == "__main__":
    fire.Fire({
        'get': get,
        'release-patch': release_patch,
        'release-minor': release_minor,
        'release-major': release_major,
        'release-production': release_production
    })
